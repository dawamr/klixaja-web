<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'homes.index');
Route::view('products', 'products.index');
Route::view('products/{id}', 'products.detail');
Route::view('orders', 'orders.index');
Route::view('profiles', 'profiles.index');
Route::view('cart', 'cart.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
