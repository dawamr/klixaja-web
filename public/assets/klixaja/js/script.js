$(document).ready(function () {
  if ($("#category").length > 0) {
    $("#category .left, #category .right").on("mouseover", function () {
      var img_boxes = $("#category .decoration_box_1");
      if ($(this).hasClass("left")) {
        var first_img_left = Math.round(
          convert_to_number(img_boxes.eq(0).css("left"))
        );
        if (first_img_left < 0) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }

      if ($(this).hasClass("right")) {
        var img_width = Math.round(convert_to_number(img_boxes.css("width")));
        var last_img_left = Math.round(
          convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
        );
        if (last_img_left > img_width * 10) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }
    });

    $("#category .left, #category .right").on("mouseleave", function () {
      $(this).animate(
        {
          opacity: 0.0,
        },
        {
          duration: "fast",
        }
      );
    });

    $("#category .left, #category .right").on("click", function () {
      var img_boxes = $("#category .decoration_box_1");
      var img_width = Math.round(convert_to_number(img_boxes.css("width")));
      if ($(this).hasClass("left")) {
        var first_img_left = Math.round(
          convert_to_number(img_boxes.eq(0).css("left"))
        );
        if (first_img_left < 0) {
          $.each(img_boxes, function (i) {
            var left = Math.round(
              (convert_to_number($(this).css("left"), true) / img_width) * 10
            );
            $(this).animate(
              {
                left: left + 10 + "%",
              },
              {
                duration: 500,
              }
            );
          });
        }
      }

      if ($(this).hasClass("right")) {
        var last_img_left = Math.round(
          convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
        );
        if (last_img_left > img_width * 10) {
          $.each(img_boxes, function (i) {
            var left = Math.round(
              (convert_to_number($(this).css("left"), true) / img_width) * 10
            );
            $(this).animate(
              {
                left: left - 10 + "%",
              },
              {
                duration: 500,
              }
            );
          });
        }
      }
    });

    $(window).on("resize", function () {
      var img_boxes = $("#category .img_box");
      for (var i = 0, j = 0; i < img_boxes.length; i += 2, j += 10) {
        var img_box1 = img_boxes.eq(i);
        var img_box2 = img_boxes.eq(i + 1);
        img_box1.css("left", j + "%");
        img_box2.css("left", j + "%");
      }
      resize();
    });

    resize();

    function resize() {
      var parent_height =
        convert_to_number($("#category").children().eq(0).css("height")) + 10;
      var category_box = convert_to_number(
        $("#category .slider_box").css("height")
      );
      var nav_arrow = $("#category .nav_arrow");
      var nav_arrow_height = convert_to_number(nav_arrow.css("height"));
      nav_arrow.css(
        "top",
        parent_height + category_box / 2 - nav_arrow_height / 2 + "px"
      );
    }
  }

  $(".heart_icon").on("click", function () {
    if ($(this).hasClass("fill")) {
      $(this).removeClass("fill");
    } else {
      $(this).addClass("fill");
    }
  });
});

function convert_to_number(value, round = false) {
  value = parseFloat(value.replace(/[^\d\-\.]/g, ""));
  if (round) {
    return Math.round(value);
  }

  return value;
}
