$(".cart").on("mouseover", function () {
  if ($(window).width() > 650) {
    $(".cart_isosceles, .cart_content").fadeIn();
  }
});

$(".cart").on("mouseleave", function () {
  $(".cart_isosceles, .cart_content").fadeOut();
});
