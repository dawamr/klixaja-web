<div id="category" class="parent sized_box col_100 row_10_90 padding_box_2wh row_gap_10">
    <h2>Kategori</h2>
    <a href="">
        <div class="child nav_arrow slider_arrow left left_1_vw circular"><i
                class="icon left_icon left_icon1"></i></div>
        <!-- update class left_icon jadi right_icon -->
        <div class="child nav_arrow slider_arrow right right_1_vw circular"><i
                class="icon right_icon right_icon1"></i></div>
        <!-- update -->
        <div class="slider_box parent sized_box col_100" style="height: 240px">
            <?php
            $translateX = 0;
            $pos = 0;
            for ($i = 0; $i < 22; $i++) {
                $rand = rand(1, 4);
                if ($i !== 0 && $i % 2 == 0) {
                    $translateX += 100;
                    $pos++;
                }
            ?>
                <div class="decoration_box_1 child sized_box" style="transform:translateX(<?php echo $translateX; ?>%)" pos="<?php echo $pos; ?>">
                    <a href="CategoryDetails">
                        <div class="decoration_img" style="background-image: url({{ asset('assets/klixaja/images/icons/kategori-'. rand(1, 8) .'.svg')  }})"></div>
                        <p class="decoration_title">Kategori <?php echo $i; ?></p>
                    </a>
                </div>
            <?php } ?>
        </div>
    </a>
</div>
