@extends('layouts.master')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/klixaja/css/slick.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/slick-carousel/slick-theme.min.css') }}">
@endsection
@section('content')
<div class="container inp mtop" style="position:relative">
    <div class="row">
        <div class="col">
            <div class="row mx-1">
                <input type="checkbox" class="mt-1 mr-1 checkbox-border" style="height: 15px; width: 15px; border-style: solid; border-width: 3px;" />
                <p class="font-weight-bold">Pilih Semua Barang</p>
                <div class="col h" style="margin-right: 20px;">
                    <a class="modalhapus" data-toggle="modal" data-target="#myModal-delete" style="text-decoration: none;" href="#"> <p class="font-weight-bold" style="text-align: right; font-size: 17px; color: green;">Hapus</p></a>
                </div>
            </div>
            <div class="col mt-2">
                <div class="linesssss" style="margin-left: -14px;"></div>
                <div class="row mt-3">
                    <input type="checkbox" class="mt-1 mr-1 checkbox-border" style="height: 15px; width: 15px;" />
                    <div class="col" style="margin: 0;">
                        <p class="font-weight-bold" style="color: orange;">Indo Shop</p>
                        <p style="margin-top: -15px;" class="clr">Kota Palembang</p>
                    </div>
                </div>
                <div class="row">
                    <input type="checkbox" class="mt-1 mr-2 checkbox-border" style="height: 15px; width: 15px;" />
                    <div class="row ml-2" style="cursor: pointer;" onclick="window.location='Details';">
                        <img class="uk_produk shadow" src="{{ asset('assets/klixaja/images/preorder3.jpg') }}" alt="" style="height: 70px; width: 70px; border-radius: 8px;" />
                        <div class="col">
                            <p class="font-weight-bold fs">Makanan Vegetarian Sate</p>
                            <p class="font-weight-bold" style="color: red;">Rp19.000</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p class="fs" style="color: rgb(189, 184, 184);">Catatan Untuk Toko(opsional)</p>
                        <textarea rows="1" cols="30" class="" type="text" style="border: none; border-bottom: 1px solid green; background: transparent; outline: none;"></textarea>
                    </div>
                    <div class="row">
                        <a href="#" style="margin-left: 10px;"> <i style="width: 25px; height: 30px; margin-right: 5px;" class="heart_icon icon heart" title="Tambahkan ke wishlist"></i></a>
                        <a href="#" class="modalhapus" data-toggle="modal" data-target="#myModal-delete" style="margin-top: 4px;"><img class="" src="{{ asset('assets/klixaja/images/icons/kotak_sampah.svg') }}" style="height: 25px; width: 25px; margin-right: 10px;" alt="" /></a>
                        <div class="stepper-input" style="margin-top: -25px;">
                            <button class="btn-input btn-left" style="border: none;">-</button> <input type="number" oninput="validity.valid||(value='');" value="1" min="1" max="100" step="1" class="input-box" />
                            <button class="btn-input btn-right" style="border: none;">+</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4 hilang">
            <div id="blnj" class="card shadow-sm">
                <div class="card-body">
                    <h6 class="card-title">Ringkasan Belanja</h6>
                    <div class="d-flex justify-content-around">
                        <div class="col"><p class="card-text">Total Harga</p></div>
                        <p class="card-text font-weight-bold" style="color: red; margin-right: 15px;">Rp150.000.000</p>
                    </div>
                    <a href="Shipping" class="btn btn-danger mt-3" style="width: 100%;">Beli(2)</a>
                </div>
            </div>
        </div>
    </div>
    <div class="linesssss mt-3" style="margin-left: 0;"></div>
    <div class="mt-3 shadow-sm lose" style="left: 0; width: 100%; position: fixed; bottom: 0; background-color: #f5f1f1; z-index: 1;">
        <div class="">
            <div class="align-items-center mt-2 ml-1 d-flex justify-content-around">
                <!-- <div class="col-4"> <button href="#" class="btn btn-default bg-grey mr-1 btn-ukrnnn" style=" font-size:12px; border: 2px solid #dfdfdf;width:140px;height:40px">Pakai Kode Promo</button> </div>-->
                <div class="col">
                    <p class="card-text fss">Total Harga</p>
                    <p class="card-text font-weight-bold fss" style="color: red; margin-right: 15px; margin-top: -16px;">Rp150.000.000</p>
                </div>
                <div class="col d-flex justify-content-end"><a href="Shipping" class="btn btn-danger mt-1 btn-ukrnnn" style="width: 150px;">Beli(2)</a></div>
            </div>
        </div>
    </div>
</div>
<div class="padding_box_2wh">
    <div style="margin-top: 20px;" class="bg" data-aos="fade-up">
        <div class="detail">
            <h2 style="color: #000;">Mungkin Anda Suka</h2>
            <p><a href="">View All</a> </p>
        </div>
    </div>
    @include('products._vertical')
</div>
<div class="padding_box_2wh">
    <div style="margin-top: 20px;" class="bg" data-aos="fade-up">
        <div class="detail">
            <h2 style="color: #000;">Rekomendasi Untuk Kamu</h2>
            <p><a href="">View All</a> </p>
        </div>
    </div>
    @include('products._vertical')
</div>
@endsection
@section('modal')
@include('cart._delete')
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $(".btn-left").on("click", function () {
            quantityField = $(this).next();
            if (quantityField.val() > 1) {
                quantityField.val(parseInt(quantityField.val(), 10) - 1);
            }
        });
        $(".btn-right").click(function () {
            quantityField = $(this).prev();
            if (quantityField.val() < 100) {
                quantityField.val(parseInt(quantityField.val(), 10) + 1);
            }
        });
    });
    var inc = document.getElementsByClassName("stepper");
    for (i = 0; i < inc.length; i++) {
        var incI = inc[i].querySelector("input"),
            id = incI.getAttribute("id"),
            min = incI.getAttribute("min"),
            max = incI.getAttribute("max"),
            step = incI.getAttribute("step");
        document.getElementById(id).previousElementSibling.setAttribute("onclick", "stepperInput('" + id + "', -" +
            step + ", " + min + ")");
        document.getElementById(id).nextElementSibling.setAttribute("onclick", "stepperInput('" + id + "', " + step +
            ", " + max + ")");
    }

    function stepperInput(id, s, m) {
        var el = document.getElementById(id);
        if (s > 0) {
            if (parseInt(el.value) < m) {
                el.value = parseInt(el.value) + s;
            }
        } else {
            if (parseInt(el.value) > m) {
                el.value = parseInt(el.value) + s;
            }
        }
    }
    var modal = $('#myModal-delete');
    var btn = $('.modalhapus');
    var ya = $('#ya');
    var tidak = $('#tidak');
    var span = $('#close-delete:first');

    btn.click(()=>{
        modal.css('display', 'none')
    });
</script>
@endsection
