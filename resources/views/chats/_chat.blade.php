<div class="container">
    <div id="btn-chat" class="bg-chat" onclick="openForm()" style="">
        <img style="width:40px;height:30px;" src="{{ asset('assets/klixaja/images/icons/chat_icon.svg') }}"
            alt="">
        <p style="margin-bottom:0;font-weight:bold">Pesan</p>
    </div>
    <div style="position: relative;">
        <div class="chat-popup" id="myForm" style="">
            <div style="display:flex;justify-content:space-between;padding:8px">
                <div style="display:flex;align-items:center">
                    <img style="width:3 0px;height:30px;margin-right:5px"
                        src="{{ asset('assets/klixaja/images/icons/chat_icon.svg') }}" alt="">
                    <p style="margin-bottom:0;font-size:18px">Pesan</p>
                </div>
                <p style="cursor:pointer;float:right;margin-bottom:0;font-size:18px" onclick="closeForm()">X</p>
            </div>

            <div style="border-radius:10px;display:grid;grid-template-columns:40% 1% 59%;margin:10px;">
                <div style="margin-right:3px;height:80%">

                    <div>
                        <form class="form-search-chat " style="">
                            <input style="padding:8px;" type="text" placeholder="Cari Nama Toko"
                                class="input-search-chat" />
                            <button style="width:40px" class="button-search-chat"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                    <div id="chat" class="multi-dropdown" style="margin-top:5px;overflow:auto;max-height:360px;">
                        <?php for ($i = 0; $i < 20; $i++) { ?>
                        <div class="chat-hover" style="margin-bottom:8px;display:flex;align-items:center;cursor:pointer;height:40px;
                  background-color:whitesmoke;padding:3px;border-radius:5px;">
                            <img style="width:30px;height:30px;border-radius:50%;margin-right:10px"
                                src="assets/klixaja/images/item3.jpg" alt="">
                            <p class="hide-text1" style="font-weight:bold;font-size:14px;margin-bottom:0">Indo Shop sjdh
                                jshd sdh
                                shjdb sjksd </p>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div style="background:gray;width:1px;"></div>

                <div style="position:relative">
                    <div style="margin-bottom:8px;display:flex;align-items:center;cursor:pointer;
                       padding:3px;box-shadow: 0 4px 2px -3px gray;width:100%">
                        <img style="width:40px;height:40px;border-radius:50%;margin-right:10px"
                            src="assets/klixaja/images/item3.jpg" alt="">
                        <p class="hide-text1" style="font-weight:bold;font-size:15px;margin-bottom:0;
                     ">Indo Shop sjdh jshd sdh shjdb sjksd </p>
                        <div class="dropdownnnnn" style="text-align:right;width:30%">
                            <p class="dot " onclick="myFunction()">
                                <div id="hapusChat" style="display:none;position:absolute;z-index:1;right:5%;
                      background:white;  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);  min-width: 130px;">
                                    <p style="text-align: left;">Hapus</p>
                                </div>
                        </div>
                    </div>
                    <div id="pesan" style="overflow:auto;max-height:320px">
                        <div style="margin:15px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;
                    border-bottom-right-radius: 25px;border:1px solid #e5e5e5;padding:8px;">
                            <p>
                                Hallo kak, apakah stoknya masih ada?
                            </p>
                            <div style="text-align:right;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>
                        <div style="margin:15px;border-bottom-right-radius: 25px;border-top-left-radius: 25px;
                    border-bottom-left-radius: 25px;background-color:#eee;padding:8px;width:auto;text-align:right">
                            <div>
                                ada kak
                            </div>
                            <div style="text-align:left;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>

                        <div style="margin:15px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;
                    border-bottom-right-radius: 25px;border:1px solid #e5e5e5;padding:8px;">
                            <p>
                                Hallo kak, apakah stoknya masih ada?
                            </p>
                            <div style="text-align:right;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>

                        <div style="margin:15px;border-bottom-right-radius: 25px;border-top-left-radius: 25px;
                    border-bottom-left-radius: 25px;background-color:#eee;padding:8px;width:auto;text-align:right">
                            <div>
                                ada kak
                            </div>

                            <div style="text-align:left;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>

                        <div style="margin:15px;border-bottom-left-radius: 25px;border-top-right-radius: 25px;
                    border-bottom-right-radius: 25px;border:1px solid #e5e5e5;padding:8px;">
                            <p>
                                Hallo kak, apakah stoknya masih ada?
                            </p>
                            <div style="text-align:right;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>

                        <div style="margin:15px;border-bottom-right-radius: 25px;border-top-left-radius: 25px;
                    border-bottom-left-radius: 25px;background-color:#eee;padding:8px;width:auto;text-align:right">
                            <div>
                                ada kak
                            </div>

                            <div style="text-align:left;margin-bottom:0;font-size:8px">10-10-2020 13:00</div>
                        </div>
                    </div>
                    <form class="form-search-chat " style="position:absolute;bottom:0;right:0;left:0">
                        <input style="padding:8px;" type="text" placeholder="Tuliskan Pesan..."
                            class="input-search-chat" />
                        <button style="width:40px" class="button-search-chat"><img style="width:20px;height:20px"
                                src="{{ asset('assets/klixaja/images/icons/send-button.svg') }}"
                                alt="">
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
