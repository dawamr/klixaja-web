@extends('layouts.master')
@section('style')

@endsection
@section('content')
<div class="container mtop" style="position: relative; margin-bottom: 1em;">
    <h3>Profil Akun</h3>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div style="position: relative; background-color: #f7f7f7; padding: 10%; border-radius: 10px;">
                <div style="text-align: center;"><img style="width: 100%; border-radius: 10px;" src="{{ asset('assets/klixaja/images/preorder1.jpg') }}" alt="" /></div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
            <table class="table">
                <tr>
                    <td>Username</td>
                    <td>Philip</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>Philip</td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td>22 Oktober 2020</td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>0897232323</td>
                </tr>
                <tr class="mb-3">
                    <td class="mr-3">Email</td>
                    <td>a@gmail.com</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>terakhir diubah</td>
                </tr>
            </table>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Ubah</button>
        </div>
    </div>
</div>
@endsection
@section('modal')
@include('profiles._edit')
@endsection
@section('script')

@endsection
