<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Profil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group"><label for="staticEmail" class="col-form-label">Username</label> <input type="text" readonly class="form-control" id="staticEmail" value="aa" /></div>
                    <div class="form-group"><label for="formGroupExampleInput">Nama</label> <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Philip" /></div>
                    <div class="form-group"><label for="example-date-input" class="col-form-label">Tanggal Lahir</label> <input class="form-control" type="date" id="example-date-input" /></div>
                    <div class="form-group"><label for="inputPassword" class="col-form-label">Telepon</label> <input type="number" class="form-control" id="inputPassword" placeholder="08972363" /></div>
                    <div class="form-group"><label for="inputPassword" class="col-form-label">Email</label> <input type="email" class="form-control" id="inputPassword" placeholder="Email" /></div>
                    <div class="form-group"><label for="inputPassword" class="col-form-label">Password</label> <input type="password" class="form-control" id="inputPassword" placeholder="Password" /></div>
                    <div class="form-group"><label for="exampleFormControlFile1">Foto Profile</label> <input type="file" class="form-control-file" id="exampleFormControlFile1" /></div>
                    <button type="button" class="btn btn-success">Ubah</button>
                </form>
            </div>
        </div>
    </div>
</div>
