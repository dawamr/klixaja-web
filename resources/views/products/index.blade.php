@extends('layouts.master')
@section('style')

@endsection

@section('content')
<div class="container ">
    <h2 class="text-center myMTOP">Produk Terlaris</h2>
    @include('products._vertical')
</div>
<nav aria-label="..." class="d-flex justify-content-center mt-3">
    <ul class="pagination">
    <li class="page-item disabled">
        <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item active">
        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
    </li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
        <a class="page-link" href="#">Next</a>
    </li>
    </ul>
</nav>
@endsection
@section('script')
<script src="{{ asset('assets/bootstrap/js/bootstrap-input-spinner.js') }}"></script>
@include('products._js')
@endsection
