<div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tulis Ulasan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="d-flex justify-content-center">
                        <div class="star-widget">
                            <input type="radio" name="rate" id="rate-1" /> <label for="rate-1" class="fas fa-star"></label> <input type="radio" name="rate" id="rate-2" /> <label for="rate-2" class="fas fa-star"></label>
                            <input type="radio" name="rate" id="rate-3" /> <label for="rate-3" class="fas fa-star"></label> <input type="radio" name="rate" id="rate-4" /> <label for="rate-4" class="fas fa-star"></label>
                            <input type="radio" name="rate" id="rate-5" /> <label for="rate-5" class="fas fa-star"></label>
                        </div>
                    </div>
                    <div class="form-group"><label for="exampleFormControlTextarea1">Masukkan Ulasan</label> <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea></div>
                    <div class="form-group d-block"><label for="gambar">Masukkan Foto</label> <input class="form-control-file" accept="image/*" id="gambar" type="file" /></div>
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
