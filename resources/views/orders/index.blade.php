@extends('layouts.master')
@section('style')
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Test the quantity inputs" />
<link href="{{ asset('assets/klixaja/css/ulasan.css') }}" rel="stylesheet"  type="text/css"  />
@endsection
@section('content')
<div class="container mtop" style="padding-right: 20px;">
    <h3 class="">Daftar Pesanan Saya</h3>
    <div class="d-flex justify-content-between filter-flex">
        <form class="form-search-chat flex-grow-1 mr-3" style="">
            <input style="padding: 8px;" type="text" placeholder="Cari Pesanan" class="input-search-chat" /> <button style="width: 40px;" class="button-search-chat"><i class="fa fa-search"></i></button>
        </form>
        <div class="form-group mTopFilter" style="width: 130px;">
            <select class="form-control" id="exampleFormControlSelect1">
                <option>Pilih Status</option>
                <option>Belum Bayar</option>
                <option>Batal</option>
                <option>Sudah Bayar</option>
                <option>Pesanan Sampai</option>
            </select>
        </div>
    </div>
    <div class="">
        <div style="margin-bottom: 20px; width: 100%; box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 9px 0px; border-radius: 8px; padding: 10px; background: #fff;">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight"><p>22 oktober 2000</p></div>
                <div class="p-2 bd-highlight"><button class="btn btn-light" id="btRating" data-toggle="modal" data-target="#rating">Ulasan</button></div>
                <div class="p-2 bd-highlight"><button class="btn btn-light" style="background-color: rgb(82, 144, 12); color: white;" data-toggle="modal" data-target="#exampleModal">Rincian</button></div>
            </div>
            <div><hr /></div>
            <div class="d-flex hide-mtop">
                <div class="p-2 flex-fill">
                    <p>Id Pembayaran</p>
                    <p>1282394849</p>
                </div>
                <div class="p-2 flex-fill" style="border-left: 1px solid rgb(241, 241, 241); border-right: 1px solid rgb(241, 241, 241);">
                    <p>Status</p>
                    <p>Belum diBayar</p>
                </div>
                <div class="p-2 flex-fill">
                    <p>Total</p>
                    <p>Rp 1223233</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
@include('orders._rincian')
@include('orders._ulasan')
@endsection
@section('script')
<script>
    AOS.init();
        function goBack(){
        window.history.back();
    }
</script>
@endsection
