<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl max-width-modal" style="max-width: 95%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Rincian Pesanan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body hide-pmb">
                <div class="d-flex justify-content-between">
                    <div>
                        <p style="">No invoice</p>
                        <p style="font-weight: bold; color: #000;">INV1234o343</p>
                    </div>
                    <div>
                        <p>Tgl Pembelian</p>
                        <p style="color: #000;">22 Oktober 2020</p>
                    </div>
                </div>
                <div class="mt-2">
                    <div>
                        <p>Status</p>
                        <p style="color: rgb(82, 144, 12);">Sudah diBayar</p>
                    </div>
                    <hr />
                </div>
                <div class="row align-items-center">
                    <div class="col">
                        <div class="row pl-2">
                            <img style="width: 50px; height: 50px; border-radius: 8px; margin-right: 4px;" src="{{ asset('assets/klixaja/images/item1.jpg') }}" alt="" />
                            <div>
                                <p class="font-nota" style="color: #000;">makanan cepat saji</p>
                                <p class="font-nota">Indo shop</p>
                            </div>
                        </div>
                    </div>
                    <div class="col col-sm-2 col-md-2 col-lg-2"><div class="font-nota text-right">7pcs</div></div>
                    <div class="col col-sm-2 col-md-2 col-lg-2"><p class="font-nota text-right" style="color: #eb8242; font-weight: bold;">Rp350.0000</p></div>
                    <div class="col col-sm-2 col-md-2 col-lg-2"><p class="font-nota text-right" style="color: #eb8242; font-weight: bold;">Rp342222222</p></div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-6">
                        <p>Kurir</p>
                        <p style="color: #000;">JNE YES(1hari)</p>
                    </div>
                    <div class="col-6" style="text-align: right;"><p style="color: #eb8242;">Rp 44.000.000</p></div>
                </div>
                <div class="row mt-2">
                    <div class="col"><p class="font-nota">Total</p></div>
                    <div class="col text-right"><p class="font-nota" style="color: #eb8242;">Rp 1383943989999</p></div>
                </div>
                <hr />
                <div>
                    <div>
                        <p>Alamat Pengiriman</p>
                        <p style="color: #000;">
                            Dikirim kepada: asasa asasas, <br />
                            Kec. Kepala Madan <br />
                            Kabupaten Buru Selatan, Maluku <br />
                            32423423424
                        </p>
                    </div>
                </div>
                <div class="mt-2">
                    <div>
                        <p>Metode Pembayaran</p>
                        <p style="color: #000;">Bank Bca</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
