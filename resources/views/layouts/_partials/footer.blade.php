<div class="garis"></div>

<div class="padding_box_2wh">
    <div class="info">
        <div class="belanjaaa">
            <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt">
                        0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"
                        class="fas fa-envelope"></i> klix@gmail.com</a></p>

        </div>
        <div class="belanja">
            <h2>Pembeli</h2>
            <p><a href="">Panduan Belanja</a></p>
            <p><a href="">Pembatalan dan Pengembalian</a></p>
        </div>
        <div class="belanja">
            <h2>Penjual</h2>
            <p><a href="">Alasan Jadi Penjual</a></p>
            <p><a href="">Cara Menjadi Penjual</a></p>
            <p><a href="">Ketentuan Produk</a></p>
            <p><a href="">Ketentuan Penjual</a></p>

        </div>

        <div class="belanja">
            <h2>Klixaja</h2>
            <p><a href="">Tentang Kami</a></p>
            <p><a href="VisiMisi">Visi & Misi</a></p>
            <p><a href="">Syarat & Ketentuan</a></p>
            <p><a href="">FAQs</a></p>
            <p><a href="">Blog</a></p>
        </div>
        <div class="belanja">
            <h2>Download Aplikasi</h2>
            <a href=""><img src="{{ asset('assets/klixaja/images/icons/google_play.png') }}"
                    alt=""></a>
            <a href=""><img src="{{ asset('assets/klixaja/images/icons/ios.png') }}"
                    alt=""></a>
        </div>
    </div>
</div>

<!-- garis===================================================================== -->

<div class="garis" style="margin-top: 70px;"></div>

<div class="padding_box_2wh">
    <div class="kolom2">
        <div class="pengiriman">
            <h2>Pengiriman</h2>
            <img src="{{ asset('assets/klixaja/images/icons/icon_pengiriman.jpeg') }}" alt="">
        </div>

        <div class="pembayaran">
            <h2>Pembayaran</h2>
            <img src="{{ asset('assets/klixaja/images/icons/icon_pembayaran.jpeg') }}" alt="">
        </div>
    </div>

</div>

<div class="garis"></div>

<div class="padding_box_2wh">
    <div class="dis">
        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>
    </div>
</div>
