<header class="header_navbar">
    <div class="navbar_list">
        <div class="navbar_top">
            <div class="navbar_top_row1">
                <a href="#" class="navbar_top_row1_link">Jual</a>
                <a href="#" class="navbar_top_row1_link">Download</a>
                <div class="navbar_top_row1_follow_us">
                    <h4 style="margin-top:5px;" class="navbar_top_row1_follow_us_title">Ikuti Kami</h4>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="ig_icon icon navbar_top_icon"></i>
                    </a>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="fb_icon icon navbar_top_icon"></i>
                    </a>
                    <!-- <a href="#" class="navbar_top_row1_follow_us_link">
                    <i class="twitter_icon icon navbar_top_icon"></i>
                </a>
                <a href="#" class="navbar_top_row1_follow_us_link">
                    <i class="line_icon icon navbar_top_icon"></i>
                </a> -->
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="wa_icon icon navbar_top_icon"></i>
                    </a>
                </div>
            </div>
            <div class="navbar_top_row3">
                <a href="#" class="navbar_top_row3_link">
                    <i class="help_icon icon navbar_top_icon"></i>
                    <span>Bantuan</span>
                </a>
            </div>
        </div>
        <!-- end navbar_top -->
        <div class="navbar_bottom1">
            <a href="#" class="nav_icon">
                <svg width="30" height="25">
                    <path d="M0,5 30,5" stroke="#fff" stroke-width="5" />
                    <path d="M0,14 30,14" stroke="#fff" stroke-width="5" />
                    <path d="M0,23 30,23" stroke="#fff" stroke-width="5" />
                </svg>
            </a>
            <div class="logo1">
                <a href="#" class="logo_link">
                    <div class="logo_image1" style="display: inline-block;"></div>
                </a>

            </div>
            <div id="dropdown " class="dropdown1">
                <img clas="image-filter" src="{{ asset('assets/klixaja/images/icons/filter_iconn.svg') }}"
                    style="width: 30px;"></img>
                <div class="dropdown-content">
                    <a href="#">Makanan & Minuman X-press</a>
                    <a href="#">Produk Pre Order</a>
                    <a href="#">Produk Terbaru</a>
                    <a href="#">Produk Terlaris</a>
                </div>
            </div>
            <div class="search_bar">
                <input type="text" id="search" placeholder="Apa yang ingin anda cari?" onclick="this.placeholder=''"
                    onblur="this.placeholder='Apa yang ingin anda cari?'" class="search_field" />
                <div class="search_icon_box">
                    <i class="search_icon icon"></i>
                </div>
            </div>
            <div class="cart">
                <i class="cart_icon icon" onclick="window.location.href='Cart';" .></i>
                <div class="cart_quantity">6</div>
                <div class="cart_isosceles" style="display:none"></div>
                <div class="cart_content" style="display:none">
                    <!-- <p class="cart_no_product">Tidak ada produk.</p> -->
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                    <div class="cart_item">
                        <img src="{{ asset('assets/klixaja/images/item1.jpg') }}"
                            class="cart_item_image">
                        <p class="cart_item_title">
                            <?php echo substr("Laborum ex fugiat cillum mollit tempor laborum excepteur do adipisicing voluptate.", 0, 50) . "..."; ?>
                        </p>
                        <p class="cart_item_price"><?php echo substr("Rp150.000.000.000", 0, 13) . "..."; ?></p>
                        <a href="#" class="cart_item_delete"></a>
                    </div>
                    <?php } ?>
                    <a href="cart" class="cart_button">Lihat Keranjang</a>
                </div>
            </div>
            <div class="notifications1">
                <i class="bell_icon icon"></i>
            </div>
            <div class="user dropdownnn" data-toogle="false">
                <i id="userIcon" class="user_icon icon" ></i>
                <div id="myDropdown" class="dropdown-contenttt">
                    <div style="height: 75px; display: flex; align-items: center; margin-left: 20px; cursor: pointer;">
                        <img style="width: 50px; height: 50px; border-radius: 50%; margin-right: 10px;" src="assets/klixaja/images/item3.jpg" alt="" />
                        <p style="font-weight: bold; font-size: 18px;">Indo Shop</p>
                    </div>
                    <hr style="margin-bottom: -3px; border-top: 1px solid rgba(0, 0, 0, 0.1);" />
                    <a class="dropdown-itema" style="height: 40px; font-size: 16px; display: flex; align-items: center;" href="PesananSaya">Pesanan Saya</a>
                    <a class="dropdown-item" style="height: 40px; font-size: 16px; display: flex; align-items: center;" href="HistoriTransaksi">History Pembelian</a>
                    <a class="dropdown-item" style="height: 40px; font-size: 16px; display: flex; align-items: center;" href="#">Registrasi Mitra</a>
                    <a class="dropdown-item" style="height: 40px; font-size: 16px; display: flex; align-items: center;" href="Profile">Edit Profile</a>
                    <a id="myBtn" class="dropdown-item" data-toggle="modal" data-target="#myModal-logout" style="height: 40px; font-size: 16px; display: flex; align-items: center;" href="#">Logout</a>
                </div>
            </div>
        </div>
        <!-- end navbar_bottom -->
    </div>
    <!-- end navbar_list -->
</header>
