<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>Klixaja</title><link href="{{asset('assets/fontawesome/css/all.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/klixaja/css/style.css')}}" rel="stylesheet" /><link href="{{asset('assets/aos/dist/aos.css')}}" rel="stylesheet" /><link href="{{asset('assets/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
        <link href="{{ asset('assets/klixaja/css/cart.css') }}" rel="stylesheet"  type="text/css"  />
        <style>
            .bentukkkkk img {
                height: 150px;
            }
            .bentukkkkk {
                height: fit-content;
            }
            .font_judul {
                margin-bottom: 0;
            }
            .image_desc {
                grid-template-rows: repeat(1, 0.334fr);
            }
        </style>
        @yield('style')
    </head>
    <body>
        <div class="content">
            @include('layouts._partials.header')
            <section class="body">
                @yield('content')
                @include('layouts._partials.footer')
            </section>
        </div>
        @include('layouts._partials._logout')
        @yield('modal')
        <script src="{{asset('assets/klixaja/js/lib/jquery-3.5.1.min.js')}}"></script>
        <script src="{{asset('assets/pooper/popper.min.js')}}"></script>
        <script src="{{asset('assets/aos/dist/aos.js')}}"></script>
        <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
        <script>
            AOS.init();
        </script>
        <script src="{{asset('assets/klixaja/js/script.js')}}"></script>
        <script src="{{asset('assets/klixaja/js/nav.js')}}"></script>
        <script src="{{asset('assets/klixaja/js/slick.min.js')}}"></script>
        <script src="{{ asset('assets/klixaja/js/cartHover.js') }}"></script>
        <script>
            var modal = document.getElementById("myModal-logout");
            var btn = document.getElementById("myBtn");
            var ya = document.getElementById("yaa");
            var tidak = document.getElementById("tidakk"); // Get the <span> element that closes the modal var span=document.getElementsByClassName("close-logout")[0]; // When the user clicks the button, open the modal btn.onclick=function (){modal.style.display="block"; document.getElementById("myDropdown").style.display="none";}; // document.getElementById('userIcon').onclick=function(){// document.getElementById('myDropdown').style.display="block"; //}// When the user clicks on <span> (x), close the modal span.onclick=function (){modal.style.display="none";}; ya.onclick=function (){modal.style.display="none";}; tidak.onclick=function (){modal.style.display="none";}; // When the user clicks anywhere outside of the modal, close it window.onclick=function (event){if (event.target==modal){document.getElementById("myModal-logout").style.display="none";}};
        </script>
        <script>
            $(".dropdownnn:first").click(() => {
                let toogle = $(".dropdownnn:first").attr("data-toogle");
                if (toogle == "true") {
                    $("#myDropdown").removeClass("showww");
                    $(".dropdownnn:first").attr("data-toogle", "false");
                } else {
                    $("#myDropdown").addClass("showww");
                    $(".dropdownnn:first").attr("data-toogle", "true");
                }
            });
        </script>

        @yield('script')

    </body>
</html>
