@extends('layouts.master')

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/klixaja/css/slick.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/slick-carousel/slick-theme.min.css') }}">
<style>
    .carousel {
        position: unset;
    }
    /* .slider_box {
        overflow-y: hidden;
    } */
</style>
@endsection

@section('content')

    @include('homes._carousel')

    @include('categories._category')

    <div class="padding_box_2wh">
        <div class="bg" data-aos="fade-up">
            <div class="detail">
                <h1 style="color: #000;">Produk Pre Order</h1>
                <p><a href="ViewAll">View All</a> </p>
            </div>
        </div>
        <div style="margin-left: 0px;">
            @include('products._horizontal')
        </div>
    </div>

    <div class="padding_box_2wh">
        <div class="bg" data-aos="fade-up">
            <div class="detail">
                <h1 style="color: #000;">Produk Pre Order</h1>
                <p><a href="ViewAll">View All</a> </p>
            </div>
        </div>
        <div style="margin-left: 0px;">
            @include('products._horizontal')
        </div>
    </div>

    @include('banners._banner')

    <div class="padding_box_2wh">
        <div style="margin-top: 20px;" class="bg" data-aos="fade-up">
            <div class="detail">
                <h1 style="color: #000;">Produk Terbaru</h1>
                <p><a href="">View All</a> </p>
            </div>
        </div>
        @include('products._vertical')
    </div>

    <div class="padding_box_2wh">
        <div class="bg" data-aos="fade-up">
            <div class="detail">
                <h1 style="color: #000;">Terlaris</h1>
                <p><a href="">View All</a> </p>
            </div>
        </div>

        <div style="margin-left: 10px;">
            @include('products._horizontal')
        </div>
    </div>

    @include('chats._chat')
@endsection

@section('script')
    <script src="{{asset('assets/klixaja/js/home.js')}}"></script>
    @include('chats._js')
    @include('products._js')
@endsection
