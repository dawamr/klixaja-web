<div class="home_banners padding_box_2wh">
    <div class="parent left_banner">
        <div class="child nav_arrow carousel_arrow left">&#10094;</div>
        <div class="child nav_arrow carousel_arrow right">&#10095;</div>
        <div class="carousel">
            <div class="carousel_img active"
                style="background-image:url({{ asset('assets/klixaja/images/banner'. rand(1,3) .'.jpeg') }}); left: 0;"
                pos="0"></div>
            <div class="carousel_img"
                style="background-image:url({{ asset('assets/klixaja/images/banner'. rand(1,3) .'.jpeg') }}); left: 100%"
                pos="1"></div>
            <div class="carousel_img"
                style="background-image:url({{ asset('assets/klixaja/images/banner'. rand(1,3) .'.jpeg') }}); left: 200%"
                pos="2"></div>
        </div>
    </div>
    <div class="right_banner">
        <div class="right_banner_img"
            style="background-image:url({{ asset('assets/klixaja/images/banner'. rand(1,3) .'.jpeg') }})"></div>
        <div class="right_banner_img"
            style="background-image:url({{ asset('assets/klixaja/images/banner'. rand(1,3) .'.jpeg') }})"></div>
    </div>
</div>
